/*
 *  PlayState.h
 *  Normal "play" state
 *
 *  Created by Marcelo Cohen on 08/13.
 *  Copyright 2013 PUCRS. All rights reserved.
 *
 */

#ifndef PLAY_STATE_H_
#define PLAY_STATE_H_

#include "GameState.h"
#include "Sprite.h"
#include "InputManager.h"
#include <tmx/MapLoader.h>

class PlayState : public cgf::GameState
{
    public:

    void init();
    void cleanup();

    void pause();
    void shoot();
    void resume();


    enum { RIGHT=0, LEFT, UP, DOWN };
    std::string walkStates[4];
    int currentDir;
    enum { YELLOW=0, GREY, GREEN, PURPLE };
    std::string tankColor[4];
    int currentTankColor;

    int tankLevel;

    void handleEvents(cgf::Game* game);
    void update(cgf::Game* game);
    void draw(cgf::Game* game);

    // Implement Singleton Pattern
    static PlayState* instance()
    {
        return &m_PlayState;
    }

    protected:

    PlayState() {}

    private:

    sf::SoundBuffer beginSoundBuffer;
    sf::Sound beginSound;
    sf::SoundBuffer pauseSoundBuffer;
    sf::Sound pauseSound;
    sf::SoundBuffer shootSoundBuffer;
    sf::Sound shootSound;

    static PlayState m_PlayState;

    int dirx, diry;
    cgf::Sprite player;
    sf::RenderWindow* screen;
    cgf::InputManager* im;

    tmx::MapLoader* map;

    // Checks collision between a sprite and a map layer
    bool checkCollision(uint8_t layer, cgf::Game* game, cgf::Sprite* obj);

    // get a cell GID from the map (x and y in world coords)
    sf::Uint16 getCellFromMap(uint8_t layernum, float x, float y);

};

#endif
