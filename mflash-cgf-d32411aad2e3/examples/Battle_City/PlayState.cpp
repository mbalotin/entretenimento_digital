/*
 *  PlayState.cpp
 *  Normal "play" state
 *
 *  Created by Marcelo Cohen on 08/13.
 *  Copyright 2013 PUCRS. All rights reserved.
 *
 */

#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <string.h>
#include "Game.h"
#include "PlayState.h"
#include "InputManager.h"

#include <signal.h>
#include <stdio.h>
#include <sys/time.h>

PlayState PlayState::m_PlayState;

using namespace std;

bool canLevelUpgrade = true;

bool starting = true;
bool paused = false;

void timer_handler (int signum)
{
    canLevelUpgrade = true;
}

void setTimer(){
    struct sigaction sa;
    struct itimerval timer;

    /* Install timer_handler as the signal handler for SIGVTALRM. */
    memset (&sa, 0, sizeof (sa));
    sa.sa_handler = &timer_handler;
    sigaction (SIGVTALRM, &sa, NULL);

    /* Configure the timer to expire after 1 sec... */
    timer.it_value.tv_sec = 1;
    timer.it_value.tv_usec = 0;
    /* ... and every 1000 msec after that. */
    timer.it_interval.tv_sec = 1;
    timer.it_interval.tv_usec = 0;
    /* Start a virtual timer. It counts down whenever this process is
    *    executing. */
    setitimer (ITIMER_VIRTUAL, &timer, NULL);
}

void PlayState::init()
{
    //player.load("data/img/tank.png",125,125, 15,15,0,0,0,0);

    walkStates[0] = "-walk-right";
    walkStates[1] = "-walk-left";
    walkStates[2] = "-walk-up";
    walkStates[3] = "-walk-down";
    currentDir = RIGHT;
    tankColor[0] = "1-"; // yellow
    tankColor[1] = "2-"; // grey
    tankColor[2] = "3-"; // red
    tankColor[3] = "4-"; // purple
    currentTankColor = YELLOW;

    tankLevel = 1;

    player.load("data/img/tank.png",16,16,0,0,0,0,8,8,8);
    player.setPosition(8,8);

    player.loadAnimation("data/img/tankanimation.xml");

    map = new tmx::MapLoader("data/maps");       // all maps/tiles will be read from data/maps
    // map->AddSearchPath("data/maps/tilesets"); // e.g.: adding more search paths for tilesets
    map->Load("battle_city.tmx");
    // ř

    //strcat(str, );
    //strcat(str, tankLevel );
    //strcat(str, walkStates[currentDir] );

    player.setAnimation(tankColor[currentTankColor] + std::to_string(tankLevel) + walkStates[currentDir]);
    player.setAnimRate(15);
    player.setScale(1,1);
    player.play();


    dirx = 0; // direção do sprite: para a direita (1), esquerda (-1)
    diry = 0; // baixo (1), cima (-1)

    im = cgf::InputManager::instance();

    im->addKeyInput("left", sf::Keyboard::Left);
    im->addKeyInput("right", sf::Keyboard::Right);
    im->addKeyInput("up", sf::Keyboard::Up);
    im->addKeyInput("down", sf::Keyboard::Down);
    im->addKeyInput("quit", sf::Keyboard::Escape);
    im->addKeyInput("stats", sf::Keyboard::S);
    im->addKeyInput("levelUp", sf::Keyboard::Add);
    im->addKeyInput("pause", sf::Keyboard::P);
    im->addKeyInput("levelDown", sf::Keyboard::Subtract);
    im->addKeyInput("space", sf::Keyboard::Space);
    im->addMouseInput("rightclick", sf::Mouse::Right);

    setTimer();

    pauseSoundBuffer.loadFromFile("data/audio/tank/pause.ogg");
    pauseSound.setBuffer(pauseSoundBuffer);
    pauseSound.setAttenuation(0);

    beginSoundBuffer.loadFromFile("data/audio/tank/stage_start.ogg");
    beginSound.setBuffer(beginSoundBuffer);
    beginSound.setAttenuation(0);


    shootSoundBuffer.loadFromFile("data/audio/tank/bullet_shot.ogg");
    shootSound.setBuffer(shootSoundBuffer);
    shootSound.setAttenuation(0);

    beginSound.play();

    cout << "PlayState: Init" << endl;
}

void PlayState::cleanup()
{
    cout << "PlayState: Clean" << endl;
}

void PlayState::pause()
{
    if (pauseSound.getStatus() == sf::Sound::Stopped){
        pauseSound.play();
        paused = !paused;
    }
}

void PlayState::shoot(){

    if (shootSound.getStatus() == sf::Sound::Stopped){
        shootSound.play();
    }
}

void PlayState::resume()
{
    cout << "PlayState: Resumed" << endl;
}

void PlayState::handleEvents(cgf::Game* game)
{
    if(im->testEvent("pause"))
        pause();

    if (paused)
        return;
    screen = game->getScreen();
    sf::Event event;

    while (screen->pollEvent(event))
    {
        if(event.type == sf::Event::Closed)
            game->quit();
    }

    dirx = diry = 0;
    int newDir = currentDir;


    if(im->testEvent("space")) {
        shoot();
    }


    if(im->testEvent("left")) {
        dirx = -1;
        newDir = LEFT;
    }else if(im->testEvent("right")) {
        dirx = 1;
        newDir = RIGHT;
    }else if(im->testEvent("up")) {
        diry = -1;
        newDir = UP;
    }else if(im->testEvent("down")) {
        diry = 1;
        newDir = DOWN;
    }
    if(im->testEvent("levelUp") && canLevelUpgrade && tankLevel < 8) {
        tankLevel++;
        canLevelUpgrade = false;
    }
    if(im->testEvent("levelDown") && canLevelUpgrade && tankLevel > 1) {
        tankLevel--;
        canLevelUpgrade = false;
    }

    if(im->testEvent("quit") || im->testEvent("rightclick"))
        game->quit();

    if(im->testEvent("stats"))
        game->toggleStats();

    if(dirx == 0 && diry == 0) {
        player.pause();
    }
    else {
        player.play();
    }

    player.setAnimation(tankColor[currentTankColor] + std::to_string(tankLevel) + walkStates[newDir]);

    if(currentDir != newDir){
        cout << "Tank animation: "+tankColor[currentTankColor] + std::to_string(tankLevel) + walkStates[newDir] << endl;
        currentDir = newDir;
    }

    player.setXspeed(32*dirx);
    player.setYspeed(32*diry);
}

void PlayState::update(cgf::Game* game)
{
    if (!paused){
        player.update(game->getUpdateInterval());
        checkCollision(0, game, &player);
        if (starting){
            starting = false;
        }
    }
}

void PlayState::draw(cgf::Game* game)
{
    screen = game->getScreen();
    map->Draw(*screen,0);
    screen->draw(player);
    map->Draw(*screen,1);
    map->Draw(*screen,2);
}


bool PlayState::checkCollision(uint8_t layer, cgf::Game* game, cgf::Sprite* obj)
{
    int i, x1, x2, y1, y2;
    bool bump = false;

    // Get the limits of the map
    sf::Vector2u mapsize = map->GetMapSize();
    // Get the width and height of a single tile
    sf::Vector2u tilesize = map->GetMapTileSize();

    mapsize.x /= tilesize.x;
    mapsize.y /= tilesize.y;
    mapsize.x--;
    mapsize.y--;

    // Get the height and width of the object (in this case, 100% of a tile)
    sf::Vector2u objsize = obj->getSize();
    objsize.x *= obj->getScale().x;
    objsize.y *= obj->getScale().y;

    float px = obj->getPosition().x;
    float py = obj->getPosition().y;

    double deltaTime = game->getUpdateInterval();

    sf::Vector2f offset(obj->getXspeed()/1000 * deltaTime, obj->getYspeed()/1000 * deltaTime);

    float vx = offset.x;
    float vy = offset.y;

    //cout << "px,py: " << px << " " << py << endl;

    //cout << "tilesize " << tilesize.x << " " << tilesize.y << endl;
    //cout << "mapsize " << mapsize.x << " " << mapsize.y << endl;

    // Test the horizontal movement first
    i = objsize.y > tilesize.y ? tilesize.y : objsize.y;

    for (;;)
    {
        x1 = (px + vx) / tilesize.x;
        x2 = (px + vx + objsize.x - 1) / tilesize.x;

        y1 = (py) / tilesize.y;
        y2 = (py + i - 1) / tilesize.y;

        if (x1 >= 0 && x2 < mapsize.x && y1 >= 0 && y2 < mapsize.y)
        {
            if (vx > 0)
            {
                // Trying to move right

                int upRight   = getCellFromMap(layer, x2*tilesize.x, y1*tilesize.y);
                int downRight = getCellFromMap(layer, x2*tilesize.x, y2*tilesize.y);
                if (upRight || downRight)
                {
                    // Place the player as close to the solid tile as possible
                    px = x2 * tilesize.x;
                    px -= objsize.x;// + 1;
                    vx = 0;
                    bump = true;
                }
            }

            else if (vx < 0)
            {
                // Trying to move left

                int upLeft   = getCellFromMap(layer, x1*tilesize.x, y1*tilesize.y);
                int downLeft = getCellFromMap(layer, x1*tilesize.x, y2*tilesize.y);
                if (upLeft || downLeft)
                {
                    // Place the player as close to the solid tile as possible
                    px = (x1+1) * tilesize.x;
                    vx = 0;
                    bump = true;
                }
            }
        }

        if (i == objsize.y) // Checked player height with all tiles ?
        {
            break;
        }

        i += tilesize.y; // done, check next tile upwards

        if (i > objsize.y)
        {
            i = objsize.y;
        }
    }

    // Now test the vertical movement

    i = objsize.x > tilesize.x ? tilesize.x : objsize.x;

    for (;;)
    {
        x1 = (px / tilesize.x);
        x2 = ((px + i-1) / tilesize.x);

        y1 = ((py + vy) / tilesize.y);
        y2 = ((py + vy + objsize.y-1) / tilesize.y);

        if (x1 >= 0 && x2 < mapsize.x && y1 >= 0 && y2 < mapsize.y)
        {
            if (vy > 0)
            {
                // Trying to move down
                int downLeft  = getCellFromMap(layer, x1*tilesize.x, y2*tilesize.y);
                int downRight = getCellFromMap(layer, x2*tilesize.x, y2*tilesize.y);
                if (downLeft || downRight)
                {
                    // Place the player as close to the solid tile as possible
                    py = y2 * tilesize.y;
                    py -= objsize.y;
                    vy = 0;
                    bump = true;
                }
            }

            else if (vy < 0)
            {
                // Trying to move up

                int upLeft  = getCellFromMap(layer, x1*tilesize.x, y1*tilesize.y);
                int upRight = getCellFromMap(layer, x2*tilesize.x, y1*tilesize.y);
                if (upLeft || upRight)
                {
                    // Place the player as close to the solid tile as possible
                    py = (y1 + 1) * tilesize.y;
                    vy = 0;
                    bump = true;
                }
            }
        }

        if (i == objsize.x)
        {
            break;
        }

        i += tilesize.x; // done, check next tile to the right

        if (i > objsize.x)
        {
            i = objsize.x;
        }
    }

    // Now apply the movement and animation

    obj->setPosition(px+vx,py+vy);
    px = obj->getPosition().x;
    py = obj->getPosition().y;

    obj->update(deltaTime, false); // only update animation

    // Check collision with edges of map
    if (px < 0)
        obj->setPosition(px,py);
    else if (px + objsize.x >= mapsize.x * tilesize.x)
        obj->setPosition(mapsize.x*tilesize.x - objsize.x - 1,py);

    if(py < 0)
        obj->setPosition(px,0);
    else if(py + objsize.y >= mapsize.y * tilesize.y)
        obj->setPosition(px, mapsize.y*tilesize.y - objsize.y - 1);

    return bump;
}


// Get a cell GID from the map (x and y in global coords)
sf::Uint16 PlayState::getCellFromMap(uint8_t layernum, float x, float y)
{
    auto layers = map->GetLayers();
    tmx::MapLayer& layer = layers[layernum];
    sf::Vector2u mapsize = map->GetMapSize();
    sf::Vector2u tilesize = map->GetMapTileSize();
    mapsize.x /= tilesize.x;
    mapsize.y /= tilesize.y;
    int col = floor(x / tilesize.x);
    int row = floor(y / tilesize.y);
    return layer.tiles[row*mapsize.x + col].gid;
}
